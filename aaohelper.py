#!/usr/bin/python3.12
import os
from random import randint
import re
import random
import io
import json
import discord
import requests
import typing
import string
from typing import Optional
from contextlib import closing
from discord import Thread, app_commands
from discord.ext import commands
from dotenv import load_dotenv # this keeps the api_token secret, and also allows for user configs

load_dotenv()

TOKEN = os.getenv('TOKEN')

intents = discord.Intents.all()
bot = commands.Bot(intents=intents, command_prefix='!')
tree = bot.tree

random.seed(os.getrandom(128))
amount = 1

async def cast(amount, l):
    i = 0
    if(amount == 0):
        amount = 1
    for i in range(amount):
        die = randint(0,5)
        l[die] = l[die] + 1
    return l

@bot.event
async def on_connect():
    print(f'{bot.user} has connected to Discord!')
    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=f'Playing with my dice'))

@bot.event
async def on_message(message):
    if message.author == bot.user or message.author.bot:
        return

    if "<@644511391302025226>" in message.content or "<@!644511391302025226>" in message.content:
        await message.reply("Did you mean <@!948208421054865410>?")

    if re.search("aa1942calc.com/#/[a-zA-Z0-9-_]+", message.content.lower()):
        #await message.edit(suppress=True)
        await aa1942calc_com_link_in_message(message)

    if not message.content.startswith('!cp') \
    or not message.author.guild_permissions.manage_messages:
        await bot.process_commands(message)
        del message
        return

    guild_id = message.guild.id
    txt_channel = message.channel
    is_reply = message.reference is not None
    params = message.content.split(maxsplit=2 if is_reply else 3)

    # !cp help
    if len(params) < 2 or params[1] == 'help':
        e = discord.Embed(title="AAO MoveBot Help")
        e.description = f"""
            This bot can move messages in two different ways.
            *Moving messages requires to have the 'Manage messages' permission.*

            **Method 1: Using the target message's ID**
            `!cp [messageID] [optional multi-move] [#targetChannelOrThread] [optional message]`

            **examples:**
            `!cp 964656189155737620 #general`
            `!cp 964656189155737620 #general This message belongs in general.`
            `!cp 964656189155737620 +2 #general This message and the 2 after it belongs in general.`
            `!cp 964656189155737620 -3 #general This message and the 3 before it belongs in general.`
            `!cp 964656189155737620 ~964656189155737640 #general This message until 964656189155737640 belongs in general.`

            **Method 2: Replying to the target message**
            `!cp [optional multi-move] [#targetChannelOrThread] [optional message]`

            **examples:**
            `!cp #general`
            `!cp #general This message belongs in general.`
            `!cp +2 #general This message and the 2 after it belongs in general.`
            `!cp -3 #general This message and the 3 before it belongs in general.`
            `!cp ~964656189155737640 #general This message until 964656189155737640 belongs in general.`
        """
        await message.author.send(embed=e)

    # !mv [msgID] [optional multi-move] [#channel] [optional message]
    else:

        try:
            moved_msg = await txt_channel.fetch_message(message.reference.message_id if is_reply else params[1])
        except:
            await txt_channel.send('An invalid message ID was provided. You can ignore the message ID by executing the **move** command as a reply to the target message')
            return

        channel_param = 1 if is_reply else 2
        before_messages = []
        after_messages = []
        if params[channel_param].startswith(('+', '-', '~')):
            value = int(params[channel_param][1:])
            if params[channel_param][0] == '-':
                before_messages = [m async for m in txt_channel.history(limit=value, before=moved_msg)]
                before_messages.reverse()
            elif params[channel_param][0] == '+':
                after_messages = [m async for m in txt_channel.history(limit=value, after=moved_msg)]
            else:
                try:
                    await txt_channel.fetch_message(value)
                except:
                    await txt_channel.send('An invalid destination message ID was provided.')
                    return

                limit = 100
                while True:
                    found = False
                    test_messages = [m async for m in txt_channel.history(limit=limit, after=moved_msg)]
                    for i, msg in enumerate(test_messages):
                        if msg.is_system():
                            test_messages.pop(i)
                        if msg.id == value:
                            after_messages = test_messages[:i+1]
                            found = True
                            break
                        elif msg.id == txt_channel.last_message.id:
                            await txt_channel.send('Reached the latest message without finding the destination message ID.')
                            return
                    if found:
                        break
                    limit += 1000

            channel_param += 1
            leftovers = params[channel_param].split(maxsplit=1)
            dest_channel = leftovers[0]
            extra_message = f'\n\n{leftovers[1]}' if len(leftovers) > 1 else ''
        else:
            dest_channel = params[channel_param]
            extra_message = f'\n\n{params[channel_param + 1]}' if len(params) > channel_param + 1 else ''

        try:
            target_channel = message.guild.get_channel_or_thread(int(dest_channel.strip('<#').strip('>')))
        except:
            await txt_channel.send("An invalid channel or thread was provided.")
            return

        wb = None
        wbhks = await message.guild.webhooks()
        for wbhk in wbhks:
            if wbhk.name == 'AAO_MoveBot':
                wb = wbhk

        parent_channel = target_channel.parent if isinstance(target_channel, Thread) else target_channel
        if wb is None:
            wb = await parent_channel.create_webhook(name='AAO_MoveBot', reason='Required webhook for AAO_MoveBot to function.')
        else:
            if wb.channel != parent_channel:
                await wb.edit(channel=parent_channel)
        author_map = {}
        for msg in before_messages + [moved_msg] + after_messages:
            msg_content = msg.content.replace('@', '@\u200b') if '@' in msg.content else msg.content
            if msg.reactions:
                global reactionss
                reactionss = msg.reactions
            files = []
            for file in msg.attachments:
                f = io.BytesIO()
                await file.save(f)
                files.append(discord.File(f, filename=file.filename))

            if isinstance(target_channel, Thread):
                await wb.send(content=msg_content, username=msg.author.display_name, avatar_url=msg.author.avatar, embeds=msg.embeds, files=files, thread=target_channel)
#                asyncio
            else:
                send_message = await wb.send(content=msg_content, wait=True, username=msg.author.display_name, avatar_url=msg.author.avatar, embeds=msg.embeds, files=files)
                if msg.reactions:
                    for r in reactionss:
                        if not isinstance(r.emoji, discord.PartialEmoji):
                            await send_message.add_reaction(r.emoji)
            if msg.author.id not in author_map:
                author_map[msg.author.id] = msg.author
        authors = [author_map[a] for a in author_map]
        author_ids = [f"<@!{a.id}>" for a in authors]
        send_objs = []
        send_objs = authors
        if send_objs:
            for send_obj in send_objs:
                message_users = f"<@!{send_obj.id}>"
                if len(author_ids) == 1:
                    message_users = author_ids[0]
                else:
                    message_users = f'{", ".join(author_ids[:-1])}{"," if len(author_ids) > 2 else ""} and {author_ids[-1]}'
                description = "MESSAGE_USER, your message has been moved to DESTINATION_CHANNEL by MOVER_USER" \
                    .replace("MESSAGE_USER", message_users) \
                    .replace("DESTINATION_CHANNEL", dest_channel) \
                    .replace("MOVER_USER", f"<@!{message.author.id}>")
                description = f'{description}{extra_message}'
                e = discord.Embed(title="Message Moved")
                e.description = description
                await send_obj.send(embed=e)
        await bot.process_commands(message)

async def aa1942calc_com_link_in_message(message):
    await asyncio.sleep(1)
    if not message.flags.suppress_embeds:
        await message.edit(suppress=True)

@bot.event
async def on_message_edit(before, after):
    if (before.channel.id or after.channel.id) == 941854510647762954:
        return
    
    if "<@644511391302025226>" in after.content or "<@!644511391302025226>" in after.content:
        await after.reply("Did you mean <@!948208421054865410>?")

    if re.search("aa1942calc.com/#/[a-zA-Z0-9-_]+", after.content.lower()):
        #await after.edit(suppress=True)
        await aa1942calc_com_link_in_message(after)
        

    channel = bot.get_channel(941854510647762954)
    e = discord.Embed(title="Message Edited",description=f"[Jump to message](https://discord.com/channels/{before.guild.id}/{before.channel.id}/{before.id})",timestamp=discord.utils.utcnow())
    e.add_field(name="Author",value=f"{before.author.mention}",inline=True)
    e.add_field(name="Channel",value=f"{after.channel.mention}",inline=True)
    if before.attachments:
        e.add_field(name="Attachments",value=f"{len(before.attachments)}",inline=True)
    else:
        e.add_field(name="Attachments",value=f"None",inline=True)
    e.add_field(name="Before Timestamp",value=f"{before.created_at}",inline=True)
    e.add_field(name="After Timestamp",value=f"{after.edited_at}",inline=True)
    if before.content:
        if len(before.content) <= 1024:
            e.add_field(name="Before Content",value=f"{before.content}",inline=False)
        else:
            short = before.content[0 : 1023] + "…"
            e.add_field(name="Before Content, abbr.",value=short,inline=False)
    else:
        e.add_field(name="No Before Content",value="Was this an embed?",inline=False)
    if after.content:
        if len(after.content) <= 1024:
            e.add_field(name="After Content",value=f"{after.content}",inline=False)
        else:
            short = after.content[0 : 1023] + "…"
            e.add_field(name="After Content, abbr.",value=short,inline=False)
    else:
        e.add_field(name="No After Content",value="Was this an embed?",inline=False)
    e.set_footer(text=f"Author: {before.author.id} • Channel {before.channel.id} • Message: {before.id}")
    e.set_thumbnail(url=before.author.display_avatar)
    await channel.send(embed=e)

@bot.event
async def on_message_delete(message):
    channel = bot.get_channel(941862114727960688)
    e = discord.Embed(title="Message Deleted",timestamp=discord.utils.utcnow())
    e.add_field(name="Author",value=f"{message.author.mention}",inline=True)
    e.add_field(name="Channel",value=f"{message.channel.mention}",inline=True)
    e.add_field(name="Message Timestamp",value=f"{message.created_at}",inline=True)
    if message.content:
        if len(message.content) <= 1024:
            e.add_field(name="Content",value=f"{message.content}",inline=False)
        else:
            short = message.content[0 : 1024]
            e.add_field(name="Abbreviated Content",value=short,inline=False)
    else:
        e.add_field(name="No Message Content",value="Was this an embed?",inline=False)
    if message.attachments:
        if len(message.attachments) == 1:
            e.add_field(name="One Attachment",value="Below",inline=True)
            e.set_image(url=message.attachments[0].url)
        else:
            e.add_field(name=f"{len(message.attachments)} Attachments Below",value=":point_down: ",inline=True)
    else:
        e.add_field(name="No Attachments",value="0",inline=True)
    e.set_footer(text=f"Author: {message.author.id} • Channel: {message.channel.id} • Message: {message.id}")
    e.set_thumbnail(url=message.author.display_avatar)
    foobar = await channel.send(embed=e)
    if len(message.attachments) > 1:
        for i in message.attachments:
            att = requests.get(i.url).content
            att_name = i.url.split("/")[-1]
            name = "/tmp/" + str(''.join(random.choice(string.digits) for n in range(8))) + "_" + str(att_name)
            with open(name, "wb") as handler:
                handler.write(att)
            file = discord.File(name)
            await foobar.reply(file=file)
            os.remove(name)

async def member_count_update():
    guild = bot.get_guild(606254910438375434)
    mc = guild.member_count
    mcc = guild.get_channel(1026754229756506152)
    l = str(mcc).split(' ')
    if mc == int(l[-1]):
        print('')
    else:
        await mcc.edit(name='Member Count: '+str(mc))

@bot.event
async def on_member_join(member):
    await member_count_update()
    archive = bot.get_channel(606254910438375436)
    guild = bot.get_guild(606254910438375434)
    commander_role = await guild.get_role(941829635367378944)
    await member.add_role(commander_role, reason="New Join", atomic=True)
    mc = guild.member_count
    a = discord.Embed(title=f"New User",description=f"**{member} has joined**\n\nMember Mention: {member.mention}\nMember ID: {member.id}\nMember Created At: {member.created_at}\nGuild Member Count: {mc}",timestamp=discord.utils.utcnow())
    a.set_thumbnail(url=member.display_avatar)
    await archive.send(embed=a)

    if member.id == int(969107453750947850):
        await archive.send(f"Other join message ignored for user {member.mention}")
        return

    message_pool = [
        "Feel free to introduce yourself!",
        "Konnichiwa!",
        "Guten Tag",
        "Здравствуйте",
        "Keep those weapons drawn! There's enemies in this sector",
        "Good day sir,\nMst.Sgt. <@!957702878473105438> reports 6 brigades, 143 artillery, 42 armor, 196 APCs at your command. The men are eagar to fight and awaiting your orders, right this way!" ,
        "We are surrounded. The enemy is entrenched to our north, south, east, and west. They can't get away from us now!"
    ]
    welcome_message = random.choice(message_pool)
    channel = bot.get_channel(606255419727544339)
    e = discord.Embed(title="Welcome to the A&A 1942 Online community Discord",description=welcome_message,timestamp=discord.utils.utcnow())
    e.set_footer(text="Member Count: " + str(mc))
    await channel.send(f"Welcome {member.mention}!", embed=e)

@bot.event
async def on_member_remove(member):
    await member_count_update()
    channel = bot.get_channel(606254910438375436)
    guild = bot.get_guild(606254910438375434)
    mc = guild.member_count
    e  = discord.Embed(title="Member Left",description=f"{member} has left the server! Send a search party?\n\nMember Mention: {member.mention}\nMember ID: {member.id}\nMember Created At: {member.created_at}\nGuild Member Count: {mc}",timestamp=discord.utils.utcnow())
    e.set_thumbnail(url=member.display_avatar)
    await channel.send(embed=e)

@bot.event
async def on_member_update(before, after):
    br = before.roles
    ar = after.roles

    brid = []
    arid = []
    for i in ar:
        arid.append(i.id)
    for i in br:
        brid.append(i.id)

    if 880252578477244417 in arid or 880252578477244417 in brid:
        league_org_channel = bot.get_channel(1180400708906913832)
        e = discord.Embed(title="League User Updated", description=f"User {before.name} has been updated\n{after.mention}.\n\nPlease update https://docs.google.com/spreadsheets/d/1Jd6-tCnQuxYrl1MMNW6zIjDOltZc9zYxKEiZtNzI7Oc/edit?usp=sharing as necessary")
        await league_org_channel.send(embed=e)

    if 747948388263395378 in arid and not 747948388263395378 in brid:
        political = bot.get_channel(747948622389182515)
        e = discord.Embed(title="Welcome to Political", description="This channel is for politics. You're a racist asshole / commie bitch.\nNow that the thin-skinned are weeded out: This channel is for off-topic discussion that might cause needless drama in the main channels. Contemporary political and social issues are the order of the day.\nIn here you can be as vicious and vulgar as you like, and should expect as much, with one caveat: that you not insult or condemn anyone in a non sequitur (i.e. without relevant context). This is a different standard of conduct than in the main channels.\nMods will still interpret and enforce Discord Community Guidelines at their discretion.")
        e.add_field(name="Catchphrase",value="Welcome to <#747948622389182515>, the cess pool of AAO. This is where the privates get sent when they fall asleep on post to burn the company's shit",inline=False)
        await political.send(f"<@!{before.id}>",embed=e)

    server_logs = bot.get_channel(941854510647762954)
    for b in brid:
        if not b in arid:
            e = discord.Embed(title=f"{before}",description=f"**Role Removed**\n<@&{b}>",timestamp=discord.utils.utcnow())
            e.set_thumbnail(url=before.display_avatar)
            e.set_footer(text=f"ID: {before.id}")
            await server_logs.send(f"{before.mention}",embed=e)
    for a in arid:
        if not a in brid:
            e = discord.Embed(title=f"{after}",description=f"**Role Added**\n<@&{a}>",timestamp=discord.utils.utcnow())
            e.set_thumbnail(url=after.display_avatar)
            e.set_footer(text=f"ID: {after.id}")
            await server_logs.send(f"{after.mention}",embed=e)

@tree.command()
@app_commands.checks.has_permissions(send_messages=True)
async def ping(interaction: discord.Interaction):
    await interaction.response.send_message("pong")

@tree.command()
@app_commands.checks.has_permissions(send_messages=True)
@app_commands.describe(
        amount='How many dice to roll'
        )
async def roll(interaction: discord.Interaction, amount: typing.Optional[int] = 1):
    l = [0,0,0,0,0,0]
    await cast(amount, l)
    emoji = [
        '<:dice_one:665298149274943550>',
        '<:dice_two:711992588709920779>',
        '<:dice_three:711992763209744415>',
        '<:dice_four:711992276724744253>',
        '<:dice_five:711992019349799005>',
        '<:dice_six:665298304732495892>',
    ]
    await interaction.response.send_message('\n'.join(a + str(b) for a, b in zip(emoji, l)))

@tree.command()
@app_commands.checks.has_permissions(send_messages=True)
@app_commands.describe(
        player_one='Player one\'s name',
        player_two='Player two\'s name',
        player_three='player three\'s name',
        player_four='player four\'s name',
        player_five='player five\'s name',
        )
async def sides(interaction: discord.Interaction, player_one: str, player_two: str, player_three: typing.Optional[str] = None, player_four: typing.Optional[str] = None, player_five: typing.Optional[str] = None):
    players = [player_one, player_two,player_three,player_four,player_five]
    players = [x for x in players if x]
    random.shuffle(players)
    amount = len(players)
    g = amount - 2
    sides = [
        ['<:axis:665257614002749482>','<:allies:665257668797267989>'],
        ['<:allies:665257668797267989>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>'],
        ['<:soviet_union:660218154227859457><:united_states:660218154160619533>','<:united_kingdom:660218154378854401>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>'],
        ['<:soviet_union:660218154227859457>','<:german_reich:660218154286448676>','<:united_kingdom:660218154378854401>','<:imperial_japan:660218154638901279>','<:united_states:660218154160619533>'],
    ]
    await interaction.response.send_message('\n'.join(a + b for a, b in zip(sides[g],players)))

"""
@tree.command()
@app_commands.checks.has_permissions(send_messages=True)
@app_commands.describe(
        players='Space-separated list of players')
async def fnb_sides(interaction: discord.Interaction, players: str):
    players_list = players.split(" ", 15)
    random.shuffle(players_list)
    amount = len(players_list)
    g = amount - 2
    if g <= 5:
        sides = [
            ['<:axis:665257614002749482>','<:allies:665257668797267989>'], # 2 players
            ['<:allies:665257668797267989>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>'], # 3 players
            ['<:soviet_union:660218154227859457><:united_states:660218154160619533>','<:united_kingdom:660218154378854401>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>'], # 4 players
            ['<:soviet_union:660218154227859457>','<:german_reich:660218154286448676>','<:united_kingdom:660218154378854401>','<:imperial_japan:660218154638901279>','<:united_states:660218154160619533>'], # 5 players
        ]
        await interaction.response.send_message('\n'.join(a + b for a, b in zip(sides[g],players_list)))
    elif g > 5 and g <= 10:
        game_one = players_list[]
        sides = [
            [['<:allies:665257668797267989>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>'],['<:allies:665257668797267989>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>']], # 6 players
            [['<:soviet_union:660218154227859457><:united_states:660218154160619533>','<:united_kingdom:660218154378854401>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>'],['<:allies:665257668797267989>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>']], # 7 players
            [['<:soviet_union:660218154227859457><:united_states:660218154160619533>','<:united_kingdom:660218154378854401>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>'],['<:soviet_union:660218154227859457><:united_states:660218154160619533>','<:united_kingdom:660218154378854401>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>']], # 8 players
            [['<:soviet_union:660218154227859457>','<:german_reich:660218154286448676>','<:united_kingdom:660218154378854401>','<:imperial_japan:660218154638901279>','<:united_states:660218154160619533>'],['<:soviet_union:660218154227859457><:united_states:660218154160619533>','<:united_kingdom:660218154378854401>','<:german_reich:660218154286448676>','<:imperial_japan:660218154638901279>']], # 9 players
            [['<:soviet_union:660218154227859457>','<:german_reich:660218154286448676>','<:united_kingdom:660218154378854401>','<:imperial_japan:660218154638901279>','<:united_states:660218154160619533>'],['<:soviet_union:660218154227859457>','<:german_reich:660218154286448676>','<:united_kingdom:660218154378854401>','<:imperial_japan:660218154638901279>','<:united_states:660218154160619533>']], # 10 players
        ]
        await interaction.response.send_message('\n'.join(a + b for a, b in zip(sides[g],players_list)))
    elif g > 10 and g <= 15:
        sides = [
            [], # 11 players
            [], # 12 players
            [], # 13 players
            [], # 14 players
            [], # 15 players
        ]
        await interaction.response.send_message('\n'.join(a + b for a, b in zip(sides[g],players_list)))
"""

@tree.command()
@app_commands.checks.has_permissions(send_messages=True)
@app_commands.describe(
        choice_one='first option',
        choice_two='second option',
        choice_three='third option',
        choice_four='fourth option',
        choice_five='fifth option',
        )
async def random_choice(interaction: discord.Interaction, choice_one: str, choice_two: str, choice_three: typing.Optional[str] = None, choice_four: typing.Optional[str] = None, choice_five: typing.Optional[str] = None):
    choices = [choice_one, choice_two, choice_three, choice_four, choice_five]
    choices = [x for x in choices if x]
    res = random.choice(choices)
    await interaction.response.send_message(f"Winner: {res}\nParticipants: {choices}",)

@tree.command()
@app_commands.checks.has_permissions(kick_members=True)
@app_commands.describe(
        title='Title for announcement',
        description='text for the announcement',
        )
async def announce(interaction: discord.Interaction, title: typing.Optional[str] = None, description: typing.Optional[str] = None):
    g = interaction.guild
    announcement_channel = bot.get_channel(610895694206861364)
    e = discord.Embed(title=title, description=description)
    await announcement_channel.send(embed=e)
    await interaction.response.send_message("Done")

@bot.command()
@commands.guild_only()
@commands.is_owner()
async def sync(ctx):
    ctx.bot.tree.copy_global_to(guild=ctx.guild)
    synced = await ctx.bot.tree.sync(guild=ctx.guild)
    await ctx.message.reply(f"Synced {len(synced)} with guilds")

#end
bot.run(TOKEN)


