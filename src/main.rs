use anyhow::{
    Context,
    Result
};
use futures_util::{
    StreamExt
};
use regex_lite::Regex;
use sqlx::{
    migrate::MigrateDatabase,
    sqlite::SqlitePool,
    Sqlite
};
use std::{
    env,
    sync::Arc,
};
use twilight_cache_inmemory::InMemoryCacheBuilder;
use twilight_gateway::{
    stream::{
        self,
        ShardEventStream
    },
    Config
};
use twilight_http::Client;
use twilight_model::{
    gateway::{
        Intents,
        payload::outgoing::update_presence::UpdatePresencePayload,
        presence::{
            ActivityType::Playing,
            MinimalActivity,
            Status
        }
    },
    id::Id
};
mod events;
mod set_commands;

const DATABASE_URL: &str = "sqlite://aaohelper.db";

#[tokio::main]
async fn main() -> Result<()> {

    tracing_subscriber::fmt::init();
    let token = env::var("DISCORD_TOKEN").context("DISCORD_TOKEN environment variable not set")?;
    let cache = Arc::new(InMemoryCacheBuilder::new().message_cache_size(1000).build());
    let client = Arc::new(Client::new(token.clone()));
    let config = Config::builder(token, Intents::all())
        .presence(UpdatePresencePayload::new(vec![MinimalActivity {
            kind: Playing,
            name: "with the dice".into(),
            url: None
        }.into()],
        false, 0, Status::DoNotDisturb)?)
        .large_threshold(250)
        .build();
    let pattern = Regex::new(r"aa1942calc\.com/#/[\w\-]").unwrap();

    let application = &client.clone().current_user_application().await?.model().await?;
    let interaction_client = client.interaction(application.id);

    tracing::info!("logged in as {} with id {}", application.name, application.id);

    if let Err(error) = set_commands::set_commands(interaction_client).await {
        tracing::error!(?error, "Failed to set application commands")
    }

    if !Sqlite::database_exists(DATABASE_URL).await.unwrap_or(false) {
        match Sqlite::create_database(DATABASE_URL).await {
            Ok(_) => tracing::info!("Database created"),
            Err(e) => tracing::error!("Error creating database: {}", e),
        }
    } else {
        tracing::info!("Database already exists");
    }

    let pool = Arc::new(SqlitePool::connect(&DATABASE_URL)
        .await
        .unwrap());

    let result = sqlx::query("CREATE TABLE IF NOT EXISTS role_info (
        id TEXT NOT NULL UNIQUE,
        info TEXT,
        added_by TEXT,
        date_updated TEXT NOT NULL)"
    ).execute(pool.clone().as_ref()).await;

    match result {
        Ok(_) => tracing::info!("database created or already exists"),
        Err(e) => println!("Error creating table: {}", e)
    }

    // Start gateway shards.
    let mut shards = stream::create_recommended(&client.clone(), config, |_id, builder| builder.build())
        .await?
        .collect::<Vec<_>>();

    let mut stream = ShardEventStream::new(shards.iter_mut());

    // Process Discord events (see `process.rs` file).
    while let Some((_shard, event)) = stream.next().await {
        let event = match event {
            Ok(event) => event,
            Err(error) => {
                if error.is_fatal() {
                    tracing::error!(?error, "fatal error while receiving event");
                    break;
                }

                // This just uses the bot's DM channel to me directly when there's an error
                let admin_dm_channel = &client.clone().create_private_channel(Id::new(948208421054865410)).await?.model().await?.id;
                client.create_message(admin_dm_channel.to_owned()).content(&*format!("**Error Received**:\n\n{}", error.to_string()))?.await?;

                tracing::warn!(?error, "error while receiving event");
                continue;
            }
        };

        tokio::spawn(events::handler::process_events(event.clone(), client.clone(), cache.clone(), pool.clone(), pattern.clone()));

    }
    Ok(())
}
