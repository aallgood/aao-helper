use anyhow::Result;
use std::{
    sync::Arc,
};
use sqlx::{
    Pool,
    Sqlite
};
use twilight_http::Client;
use twilight_model::{
    id::{
        Id,
        marker::ChannelMarker
    },
    gateway::payload::incoming::RoleDelete
};

pub async fn role_delete(role: &RoleDelete, client: &Arc<Client>, pool: Arc<Pool<Sqlite>>) -> Result<()> {

    let role_id = role.role_id.get().to_string();
    println!("{}", &role_id);

    if sqlx::query("DELETE FROM role_info WHERE id = ?")
        .bind(&role_id)
        .execute(pool.clone().as_ref())
        .await?
        .rows_affected() != 1 {
        tracing::error!("Role was not deleted: {}", &role_id)
    }
    else {
        tracing::info!("Role was deleted: {}", &role_id);
    }

    let server_commands_channel: Id<ChannelMarker> = Id::new(670090977356021780);
        if client
            .create_message(server_commands_channel)
            .content(format!("Role Deleted: <@!{}>\nRole ID: {}\n\nThe data has been removed from the database", &role_id, &role_id).as_str())?
            .await?
            .status()
            .is_success()
        {
            tracing::info!("role_delete completed: {:?}", &role);
        }
    else {
        tracing::error!("role was delete from database but message was not sent: {}", &role_id)
    }

    Ok(())
}