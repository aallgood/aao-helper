use anyhow::Result;
use std::{
    collections::BTreeSet,
    ops::Add,
    sync::Arc
};
use twilight_cache_inmemory::InMemoryCache;
use twilight_http::Client;
use twilight_model::{
    channel::message::Embed,
    gateway::payload::incoming::MemberUpdate,
    id::{
        Id,
        marker::RoleMarker
    }
};
use twilight_util::{
    builder::embed::{
        EmbedBuilder,
        EmbedFieldBuilder
    },
    snowflake::Snowflake
};

pub async fn member_update(member: &Box<MemberUpdate>, client: &Arc<Client>, cache: &Arc<InMemoryCache>) -> Result<()> {
    let old_roles = &cache.clone().member(member.clone().guild_id, member.clone().user.id).unwrap().roles().to_owned();
    let new_roles = member.clone().roles;

    let mut old_ids = BTreeSet::new();
    for id in old_roles.iter() {
        old_ids.insert(id.to_owned());
    }

    let mut new_ids = BTreeSet::new();
    for id in new_roles.iter() {
        new_ids.insert(id.to_owned());
    }

    let politician_role: Id<RoleMarker> = Id::new(747948388263395378);

    if !old_roles.contains(&politician_role) && new_roles.contains(&politician_role) {
        let e = EmbedBuilder::new()
            .title("Welcome to Political")
            .description("This channel is for politics. You're a racist asshole / commie bitch.\nNow that the thin-skinned are weeded out: This channel is for off-topic discussion that might cause needless drama in the main channels. Contemporary political and social issues are the order of the day.\nIn here you can be as vicious and vulgar as you like, and should expect as much, with one caveat: that you not insult or condemn anyone in a non sequitur (i.e. without relevant context). This is a different standard of conduct than in the main channels.\nMods will still interpret and enforce Discord Community Guidelines at their discretion.")
            .field(EmbedFieldBuilder::new("Catchphrase", "Welcome to <#747948622389182515>, the cess pool of AAO. This is where the privates get sent when they fall asleep on post to burn the company's shit"))
            .validate()?
            .build();
        client
            .create_message(Id::new(747948622389182515))
            .content(&*format!("<@!{}>", &member.clone().user.id.id()))?
            .embeds(&[e])?
            .await?;
    }

    let id_in_old_not_in_new: BTreeSet<Id<RoleMarker>> = old_ids.difference(&new_ids).cloned().collect();
    let id_in_new_not_in_old: BTreeSet<Id<RoleMarker>> = new_ids.difference(&old_ids).cloned().collect();

    let mut e: EmbedBuilder = EmbedBuilder::new()
        .title("Member Updated")
        .description(format!("<@!{}>", member.user.id.id()));
    let mut removed_ids: String = "".to_string();
    for id in id_in_old_not_in_new {
        removed_ids = removed_ids.add(format!(" <@&{}>", &id.to_string().as_str()).as_str());
    }
    if removed_ids != "".to_string() {
        e = e
            .field(EmbedFieldBuilder::new("Removed Roles:", format!("{}", removed_ids)));
    }
    //let ids = id_in_new_not_in_old;

    let mut added_ids: String = "".to_string();
    for id in id_in_new_not_in_old {
        added_ids = added_ids.add(format!(" <@&{}>", &id.to_string().as_str()).as_str());
    }
    if added_ids != "".to_string() {
        e = e
            .field(EmbedFieldBuilder::new("Added Roles:", format!("{}", added_ids)));
    }
    let e: Embed = e
        .validate()?
        .build();
    client
        .create_message(Id::new(941854510647762954))
        .embeds(&[e])?
        .await?;
    Ok(())
}