use anyhow::Result;
use std::sync::Arc;
use twilight_http::Client;
use twilight_model::{
    gateway::payload::incoming::MemberRemove,
    id::Id
};
use twilight_util::builder::embed::EmbedBuilder;

pub async fn member_remove(member: &MemberRemove, client: &Arc<Client>) -> Result<()> {
    let count = &client.clone().guild(member.clone().guild_id).await?.model().await.unwrap().member_count.unwrap();
    let e = EmbedBuilder::new()
        .title("Member Left")
        .description(format!("<{} has left the server! Send a search party?\n\nMember Mention: <@!{}>\nMember ID: {}\nGuild Member Count: {}", member.clone().user.name, member.clone().user.id, member.clone().user.id, count))
        .validate()?
        .build();
    client
        .create_message(Id::new(941854510647762954))
        .embeds(&[e])?
        .await?;
    Ok(())
}