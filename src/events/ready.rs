use anyhow::Result;
use twilight_model::gateway::payload::incoming::Ready;

// Event triggers when the bot receives `ready` from discord API. Basic info is logged
pub async fn ready(ready: &Box<Ready>) -> Result<()> {
    tracing::info!("{:?} received Ready. Currently in {:?} guilds. session_id: {:?}", ready.user.name.clone(), ready.guilds.clone().len(), ready.session_id.clone());
    Ok(())
}