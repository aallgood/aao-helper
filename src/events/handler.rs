// This file is responsible for `match`ing the incoming events into their respective types,
// and sending them where they are required to go

use anyhow::Result;
use regex_lite::Regex;
use std::{
    sync::Arc,
    time::Duration
};
use sqlx::{
    Pool,
    Sqlite,
    testing::TestTermination
};
use tokio::time::sleep;
use twilight_cache_inmemory::InMemoryCache;
use twilight_http::Client;
use twilight_model::gateway::event::Event;
use crate::events::{
    interaction_create::interaction_create,
    member_add::member_add,
    member_remove::member_remove,
    member_update::member_update,
    message_create::message_create,
    message_delete::message_delete,
    message_update::message_update,
    role_create::role_create,
    role_delete::role_delete,
    ready
};

pub async fn process_events(event: Event, client: Arc<Client>, cache: Arc<InMemoryCache>, pool: Arc<Pool<Sqlite>>, pattern: Regex) -> Result<()> {
    // Sort the events:
    match event {
        Event::InteractionCreate(ref interaction) => interaction_create(interaction, &client, pool).await?,
        Event::MemberAdd(ref member) => member_add(member, &client).await?,
        Event::MemberRemove(ref member) => member_remove(member, &client).await?,
        Event::MemberUpdate(ref member) => member_update(member, &client, &cache).await?,
        Event::MessageCreate(ref message) => message_create(&message, &client, pattern).await?,
        Event::MessageDelete(ref message) => message_delete(message, &client, &cache).await?,
        Event::MessageUpdate(ref message) => message_update(&message, &client, &cache, pattern).await?,
        Event::Ready(ref ready) => ready::ready(ready).await?,
        Event::RoleCreate(ref role) => role_create(role, &client, pool).await?,
        Event::RoleDelete(ref role) => role_delete(role, &client, pool).await?,
        _ => (),
    };
    // Update the cache *after* the event has been matched. This is necessary to ensure that events
    // that rely on cached data, such as `MessageUpdate` can rely on cached data
    sleep(Duration::from_millis(750)).await;
    match cache.clone().update(&event.clone()).is_success() {
        true => tracing::debug!("the event is cached: {:?}", &event),
        false => tracing::warn!("the event failed to cache: {:?}", &event)
    }
    Ok(())
}
