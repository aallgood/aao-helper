use anyhow::Result;
use regex_lite::Regex;
use std::sync::Arc;
use twilight_http::Client;
use twilight_model::{
    channel::message::MessageFlags,
    gateway::payload::incoming::MessageCreate,
    id::{
        Id,
        marker::ChannelMarker
    }
};

pub async fn message_create(message: &Box<MessageCreate>, client: &Arc<Client>, pattern: Regex) -> Result<()> {
    let server_logs_channel: Id<ChannelMarker> = Id::new(941854510647762954);
    match &message.channel_id {
        x if x != &server_logs_channel => {
            let author = &message.author;
            if !author.bot {
                if message.content.contains("<@644511391302025226>") || message.content.contains("<@!644511391302025226>") {
                    client
                        .create_message(message.channel_id)
                        .reply(message.id)
                        .content("Did you mean <@!948208421054865410>?")?
                        .await?;
                }
                if pattern.is_match(message.content.as_str()) && !message.flags.expect("f").contains(MessageFlags::SUPPRESS_EMBEDS) {
                    client
                        .update_message(message.channel_id, message.id)
                        .flags(MessageFlags::SUPPRESS_EMBEDS)
                        .await?;
                }
            }
        }

        _ => ()
    }
    Ok(())
}