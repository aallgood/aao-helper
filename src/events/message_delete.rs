use anyhow::Result;
use std::sync::Arc;
use twilight_cache_inmemory::InMemoryCache;
use twilight_http::Client;
use twilight_model::{
    channel::message::Embed,
    gateway::payload::incoming::MessageDelete,
    id::{
        Id,
        marker::ChannelMarker
    }
};
use twilight_util::{
    builder::embed::{
        EmbedBuilder,
        EmbedFieldBuilder,
        EmbedFooterBuilder
    },
    snowflake::Snowflake
};

pub async fn message_delete(message: &MessageDelete, client: &Arc<Client>, cache: &Arc<InMemoryCache>) -> Result<()> {
    let server_logs_channel: Id<ChannelMarker> = Id::new(941854510647762954);
    if &message.channel_id.id() != &server_logs_channel {

        // Added error handing for `user_exists_in_cache` so we can stop panicking on invalid unwrap()
        // 01/19/2024 @sadpuppies

        let user_exists_in_cache: bool = cache.message(message.id).is_some();
        dbg!(&user_exists_in_cache);
        dbg!(cache.message(message.id).unwrap().member().unwrap());
        match user_exists_in_cache {
            true => {
                if !cache.message(message.id).unwrap().member().unwrap().user.clone().unwrap().bot {
                    let mut e: EmbedBuilder = EmbedBuilder::new()
                        .title("Message Deleted")
                        .description(format!("[Jump to message](https://discord.com/channels/{}/{}/{})", message.clone().guild_id.unwrap(), message.clone().channel_id, message.clone().id.id().to_string()));
                    if cache.message(message.clone().id).is_some() {
                        if &cache.message(message.id.clone()).unwrap().content().len() > &1024 {
                            cache.message(message.id.clone()).unwrap().content().to_owned().truncate(1024);
                        };
                        e = e
                            .field(EmbedFieldBuilder::new("Content", format!("{}", &cache.message(message.clone().id).unwrap().content())).build())
                            .footer(EmbedFooterBuilder::new(format!("Author: {} • Channel: {} • Message: {}", &cache.message(message.clone().id).unwrap().author().id().to_string().to_owned(), message.channel_id, message.id)));
                    } else {
                        e = e
                            .footer(EmbedFooterBuilder::new(format!("Channel: {} • Message: {}", message.clone().channel_id, message.clone().id)));
                    }
                    let e: Embed = e
                        .validate()?
                        .build();
                    client
                        .create_message(server_logs_channel)
                        .embeds(&[e])?
                        .await?;
                }
            },
            false => {
                let e: Embed = EmbedBuilder::new()
                    .title("Message Deleted")
                    .description(
                        format!("A message has been deleted but it (or the `user` object) is not stored in the cache, or the user is a bot\nMessage ID: {}\nChannel ID: {}\nGuild ID: {}",
                                &message.id.id(),
                                &message.channel_id.id(),
                                &message.guild_id.unwrap().id()
                        )
                    )
                    .validate()?
                    .build();
                client
                    .create_message(server_logs_channel)
                    .embeds(&[e])?
                    .await?;

            }
        }
    }
    Ok(())
}