use anyhow::Result;
use rand::{
    distributions::{
        Distribution,
        Uniform
    },
    thread_rng
};
use std::sync::Arc;
use twilight_http::Client;
use twilight_model::{
    gateway::payload::incoming::MemberAdd,
    id::Id
};
use twilight_util::builder::embed::{
    EmbedBuilder,
    EmbedFooterBuilder
};

pub async fn member_add(member: &Box<MemberAdd>, client: &Arc<Client>) -> Result<()> {
    client.add_guild_member_role(member.clone().guild_id, member.clone().user.id, Id::new(941829635367378944)).await?;
    let count = client.guild(member.clone().guild_id).await?.model().await?.member_count.unwrap();
    let e = EmbedBuilder::new()
        .title("New Member")
        .description(format!("<{} has joined\n\nMember Mention: <@!{}>\nMember ID: {}\nDiscord User Since: <t:{}:F>\nGuild member Count: {}", member.clone().user.name, member.clone().user.id, member.clone().user.id, member.clone().member.joined_at.as_secs(), count))
        .validate()?
        .build();
    client
        .create_message(Id::new(941854510647762954))
        .embeds(&[e])?
        .await?;
    let message_pool: [String; 7] = [
        "Feel free to introduce yourself!".to_string(),
        "Konnichiwa!".to_string(),
        "Guten Tag".to_string(),
        "Здравствуйте".to_string(),
        "Keep those weapons drawn! There's enemies in this sector".to_string(),
        "Good day sir,\nMst.Sgt. <@!957702878473105438> reports 6 brigades, 143 artillery, 42 armor, 196 APCs at your command. The men are eagar to fight and awaiting your orders, right this way!".to_string(),
        "We are surrounded. The enemy is entrenched to our north, south, east, and west. They can't get away from us now!".to_string()
    ];
    let welcome_message: String = message_pool.get(Uniform::from(0..=message_pool.len()).sample(&mut thread_rng())).unwrap().to_string();
    let welcome_embed = EmbedBuilder::new()
        .title("Welcome to the A&A 1942 Online community Discord")
        .description(welcome_message)
        .footer(EmbedFooterBuilder::new(format!("{count}")))
        .validate()?
        .build();
    client
        .create_message(Id::new(606255419727544339))
        .embeds(&[welcome_embed])?
        .await?;
    Ok(())
}