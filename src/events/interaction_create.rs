// Handles Interactions such as slash commands, user context menus (right-click or tap on user)
// message context menu (right-click or tap on message)
// I'll most likely sort this into more files in the future

use anyhow::{
    bail,
    Result
};
use rand::{
    distributions::{
        Distribution,
        Uniform
    },
    prelude::SliceRandom,
    thread_rng
};
use std::{
    default::Default,
    sync::Arc,
    time::{
        SystemTime,
        UNIX_EPOCH
    }
};
use sqlx::{Pool, Row, Sqlite};
use twilight_http::Client;
use twilight_model::{
    application::interaction::{
        application_command::{
            CommandData,
            CommandOptionValue
        },
        Interaction,
        InteractionData,
        message_component::MessageComponentInteractionData,
        modal::ModalInteractionData
    },
    channel::message::{
        Component,
        component::{
            ActionRow,
            Button,
            ButtonStyle,
            TextInput,
            TextInputStyle
        },
        MessageFlags
    },
    gateway::payload::incoming::InteractionCreate,
    guild::Permissions,
    http::{
        interaction::{
            InteractionResponseData,
            InteractionResponse,
            InteractionResponseType
        },
        permission_overwrite::{
            PermissionOverwrite,
            PermissionOverwriteType
        }
    },
    id::{
        Id,
        marker::{
            ChannelMarker,
            GuildMarker,
            RoleMarker,
            UserMarker
        }
    },
    util::datetime::Timestamp
};
use twilight_util::{
    builder::{
        embed::EmbedBuilder,
        InteractionResponseDataBuilder
    },
    snowflake::Snowflake
};

pub async fn interaction_create(interaction: &Box<InteractionCreate>, client: &Arc<Client>, pool: Arc<Pool<Sqlite>>) -> Result<()> {
    match &interaction.clone().data {
        Some(InteractionData::ApplicationCommand(data)) => handle_interaction(interaction.0.clone(), *data.clone(), &client.clone(), pool).await?,
        Some(InteractionData::MessageComponent(data)) => handle_message_component(interaction.0.clone(), data.clone(), &client.clone()).await?,
        Some(InteractionData::ModalSubmit(data)) => description_added(interaction.0.clone(), client.clone(), data.clone(), pool).await?,
        _ => ()
    };
    Ok(())
}

async fn handle_interaction(
    interaction: Interaction,
    data: CommandData,
    client: &Arc<Client>,
    pool: Arc<Pool<Sqlite>>
) -> Result<()> {
    match &*data.name {
        "Assign to League" => assign_to_league(interaction, client.clone(), &data).await,
        "Delete Message" => delete_message(interaction, client.clone(), data).await,
        "Mute League" => mute_league(interaction, client.clone()).await,
        "ping" => ping(interaction, client.clone()).await,
        "role" => role(interaction, client.clone(), data, pool).await,
        "roll" => roll(interaction, client.clone(), data).await,
        "sides" => sides(interaction, client.clone(), data).await,
        "Timeout" => timeout(interaction, client.clone(), data).await,
        name => bail!("unknown command: {}", name),
    }
}

async fn handle_message_component(
    interaction: Interaction,
    data: MessageComponentInteractionData,
    client: &Arc<Client>
) -> Result<()> {
    match &data.custom_id.as_str().starts_with("undo_timeout") {
        true => undo_timeout(interaction.clone(), client.clone(), data.clone()).await?,
        false => (),
    }
    match &data.custom_id.as_str().starts_with("role_desc") {
        true => add_role_desc(interaction.clone(), client.clone(), &data.clone()).await?,
        false => ()
    }
    Ok(())
}

async fn add_role_desc(interaction: Interaction, client: Arc<Client>, data: &MessageComponentInteractionData) -> Result<()> {
    let role_id: Id<RoleMarker> = Id::new(data.custom_id.get(10..).unwrap().parse().unwrap());

    let component = Component::ActionRow(ActionRow {
        components: vec![
            Component::TextInput(TextInput {
                custom_id: data.custom_id.to_string(),
                label: "description".to_string(),
                max_length: Some(1024),
                min_length: Some(20),
                placeholder: None,
                required: None,
                style: TextInputStyle::Paragraph,
                value: None,
            })
        ]
    });

    client
        .interaction(interaction.application_id)
        .create_response(interaction.id, &interaction.token, &InteractionResponse {
            kind: InteractionResponseType::Modal,
            data: Some(InteractionResponseData {
                components: Some(vec![component]),
                custom_id: Some(format!("some_desc_{}", role_id).to_string()),
                title: Some(String::from("modal")),
                ..Default::default()
            }),
        })
        .await?;

    Ok(())
}

async fn assign_to_league(interaction: Interaction, client: Arc<Client>, data: &CommandData) -> Result<()> {
    let target_id: Id<UserMarker> = data.target_id.unwrap().cast();
    let league_role: Id<RoleMarker> = Id::new(880252578477244417);
    let guild: Id<GuildMarker> = Id::new(606254910438375434);
    if interaction.member.clone().unwrap().roles.contains(&Id::new(943827276104097842)) {
        let target_member = client.guild_member(guild, Id::new(target_id.get())).await?.model().await?;
        if !target_member.roles.contains(&league_role) {
            client
                .add_guild_member_role(
                    guild,
                    target_id,
                    league_role
                )
                .await?;
            client.interaction(interaction.application_id).create_response(
                interaction.id,
                &interaction.token,
                &InteractionResponse {
                    kind: InteractionResponseType::ChannelMessageWithSource,
                    data: Some(InteractionResponseDataBuilder::new()
                        .content(format!("<@!{}> assigned to League", target_id.get()))
                        .flags(MessageFlags::EPHEMERAL)
                        .build()
                    )
                }
            )
                .await?;
        }
        else {
            client
                .remove_guild_member_role(
                    guild,
                    target_id,
                    league_role
                )
                .await?;
            client.interaction(interaction.application_id).create_response(
                interaction.id,
                &interaction.token,
                &InteractionResponse {
                    kind: InteractionResponseType::ChannelMessageWithSource,
                    data: Some(InteractionResponseDataBuilder::new()
                        .content(format!("<@!{}> removed from League", target_id.get()))
                        .flags(MessageFlags::EPHEMERAL)
                        .build()
                    )
                }
            )
                .await?;
        }
    }
    else {
        client.interaction(interaction.application_id).create_response(
            interaction.id,
            &interaction.token,
            &InteractionResponse {
                kind: InteractionResponseType::ChannelMessageWithSource,
                data: Some(InteractionResponseDataBuilder::new()
                    .content("Only League Organizers can use this command. **For Shame!**")
                    .flags(MessageFlags::EPHEMERAL)
                    .build()
                )
            }
        )
            .await?;
    }
    Ok(())
}

async fn delete_message(interaction: Interaction, client: Arc<Client>, data: CommandData) -> Result<()> {
    let league_org_role: Id<RoleMarker> = Id::new(874798614122234006);
    let league_channels: Vec<Id<ChannelMarker>> = vec![
        Id::new(776900323032956929), // category ID
        Id::new(915563459431764059), // league info
        Id::new(874084922636255253), // aa league
        Id::new(949425783192027217), // league results
        Id::new(948992296458784788), // grandstands
        Id::new(948991011223379968), // league showcase
        Id::new(764126284565315624) // league org
    ];
    if league_channels.contains(&interaction.clone().channel.unwrap().id) {
        if !interaction.clone().member.unwrap().roles.contains(&league_org_role) {
            client.interaction(interaction.clone().application_id).create_response(
                interaction.clone().id,
                &interaction.token,
                &InteractionResponse {
                    kind: InteractionResponseType::ChannelMessageWithSource,
                    data: Some(InteractionResponseDataBuilder::new()
                        .content("Only League Organizers can use this command. **For Shame!**")
                        .flags(MessageFlags::EPHEMERAL)
                        .build()
                    )
                }
            )
                .await?;
        } else {
            let message = data.resolved.unwrap().messages;
            if let Err(error) = client.delete_message(interaction.clone().channel.unwrap().id, message.keys().next().unwrap().cast()).await {
                tracing::error!(?error, "Failed to delete message");
                client.interaction(interaction.clone().application_id).create_response(
                    interaction.clone().id,
                    &interaction.token,
                    &InteractionResponse {
                        kind: InteractionResponseType::ChannelMessageWithSource,
                        data: Some(InteractionResponseDataBuilder::new()
                            .content("Failed to delete message")
                            .flags(MessageFlags::EPHEMERAL)
                            .build()
                        )
                    }
                )
                    .await?;
            } else {
                client.interaction(interaction.clone().application_id).create_response(
                    interaction.clone().id,
                    &interaction.token,
                    &InteractionResponse {
                        kind: InteractionResponseType::ChannelMessageWithSource,
                        data: Some(InteractionResponseDataBuilder::new()
                            .content("Message deleted")
                            .flags(MessageFlags::EPHEMERAL)
                            .build()
                        )
                    }
                )
                    .await?;
            }
        }
    }
    else {
        client.interaction(interaction.clone().application_id).create_response(
            interaction.clone().id,
            &interaction.token,
            &InteractionResponse {
                kind: InteractionResponseType::ChannelMessageWithSource,
                data: Some(InteractionResponseDataBuilder::new()
                    .content("Command must be used in league category")
                    .flags(MessageFlags::EPHEMERAL)
                    .build()
                )
            }
        )
            .await?;
    }
    Ok(())
}

async fn description_added(interaction: Interaction, client: Arc<Client>, data: ModalInteractionData, pool: Arc<Pool<Sqlite>>) -> Result<()> {
    let info = data.components.get(0).unwrap().components.get(0).unwrap().value.clone().unwrap();
    let now = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs().to_string();
    let user = interaction.clone().member.unwrap().user.unwrap().id.id().to_string();
    let role_id = data.custom_id.get(10..).unwrap().to_string();
    dbg!(&role_id);
    sqlx::query("UPDATE role_info SET info = $1, added_by = $2, date_updated = $3 WHERE id = $4")
        .bind(&info)
        .bind(&user)
        .bind(&now)
        .bind(&role_id)
        .execute(pool.clone().as_ref())
        .await?;
    let e = EmbedBuilder::new()
        .title("Role Info Updated")
        .description(format!("Role: <@&{}>\nInfo: {}\nAdded By: <@!{}>\nLast Updated: <t:{}:F>", &role_id, &info, &user, &now))
        .validate()?
        .build();
    client
        .interaction(interaction.clone().application_id)
        .create_response(interaction.id, &interaction.token, &InteractionResponse {
            kind: InteractionResponseType::ChannelMessageWithSource,
            data: Some(InteractionResponseData {
                allowed_mentions: None,
                attachments: None,
                choices: None,
                components: None,
                content: None,
                custom_id: None,
                embeds: Some(vec![e]),
                flags: None,
                title: None,
                tts: None,
            }),
        })
        .await?;
    Ok(())
}

async fn mute_league(interaction: Interaction, client: Arc<Client>) -> Result<()> {
    let league_org_role: Id<RoleMarker> = Id::new(874798614122234006);
    let league_channels: Vec<Id<ChannelMarker>> = vec![
        Id::new(776900323032956929), // category ID
        Id::new(915563459431764059), // league info
        Id::new(874084922636255253), // aa league
        Id::new(949425783192027217), // league results
        Id::new(948992296458784788), // grandstands
        Id::new(948991011223379968), // league showcase
        Id::new(764126284565315624) // league org
    ];
    if league_channels.contains(&interaction.clone().channel.unwrap().id) {
        if !interaction.clone().member.unwrap().roles.contains(&league_org_role) {
            client.interaction(interaction.clone().application_id).create_response(
                interaction.clone().id,
                &interaction.token,
                &InteractionResponse {
                    kind: InteractionResponseType::ChannelMessageWithSource,
                    data: Some(InteractionResponseDataBuilder::new()
                        .content("Only League Organizers can use this command. **For Shame!**")
                        .flags(MessageFlags::EPHEMERAL)
                        .build()
                    )
                }
            )
                .await?;
        } else {
            let league_channel: Id<ChannelMarker> = Id::new(874084922636255253); // aa league
            let usual_league_perms_channel = twilight_model::channel::permission_overwrite::PermissionOverwrite {
                allow: (Permissions::empty()),
                deny: (Permissions::empty()),
                id: (Id::new(606254910438375434)), // @everyone in AAO discord
                kind: twilight_model::channel::permission_overwrite::PermissionOverwriteType::Role,
            };
            let muted_league_perms_channel = twilight_model::channel::permission_overwrite::PermissionOverwrite {
                allow: Permissions::empty(),
                deny: (Permissions::SEND_MESSAGES_IN_THREADS | Permissions::SEND_MESSAGES | Permissions::CREATE_PUBLIC_THREADS),
                id: (Id::new(606254910438375434)), // @everyone in AAO discord
                kind: twilight_model::channel::permission_overwrite::PermissionOverwriteType::Role,
            };
            let usual_league_perms = PermissionOverwrite {
                allow: Option::from(Permissions::empty()),
                deny: Option::from(Permissions::empty()),
                id: (Id::new(606254910438375434)), // @everyone in AAO discord
                kind: PermissionOverwriteType::Role,
            };
            let muted_league_perms = PermissionOverwrite {
                allow: Option::from(Permissions::empty()),
                deny: Option::from(Permissions::SEND_MESSAGES_IN_THREADS | Permissions::SEND_MESSAGES | Permissions::CREATE_PUBLIC_THREADS),
                id: (Id::new(606254910438375434)), // @everyone in AAO discord
                kind: PermissionOverwriteType::Role,
            };
            let current_permissions = client.channel(league_channel).await?.model().await?.permission_overwrites.unwrap();
            if current_permissions.contains(&usual_league_perms_channel) {
                client.update_channel_permission(league_channel, &muted_league_perms).await?;
                client.interaction(interaction.clone().application_id).create_response(
                    interaction.clone().id,
                    &interaction.token,
                    &InteractionResponse {
                        kind: InteractionResponseType::ChannelMessageWithSource,
                        data: Some(InteractionResponseDataBuilder::new()
                            .content("Muted the horde")
                            .flags(MessageFlags::EPHEMERAL)
                            .build()
                        )
                    }
                )
                    .await?;
            }
            else if current_permissions.contains(&muted_league_perms_channel) {
                client.update_channel_permission(league_channel, &usual_league_perms).await?;
                client.interaction(interaction.clone().application_id).create_response(
                    interaction.clone().id,
                    &interaction.token,
                    &InteractionResponse {
                        kind: InteractionResponseType::ChannelMessageWithSource,
                        data: Some(InteractionResponseDataBuilder::new()
                            .content("Unmuted the horde. Good luck, and godspeed")
                            .flags(MessageFlags::EPHEMERAL)
                            .build()
                        )
                    }
                )
                    .await?;
            }
            else {
                client.interaction(interaction.clone().application_id).create_response(
                    interaction.clone().id,
                    &interaction.token,
                    &InteractionResponse {
                        kind: InteractionResponseType::ChannelMessageWithSource,
                        data: Some(InteractionResponseDataBuilder::new()
                            .content("Current permissions are not what I expected, I am intentionally failing this command out of an abundance of caution")
                            .flags(MessageFlags::EPHEMERAL)
                            .build()
                        )
                    }
                )
                    .await?;
            }
        }
    }
    else {
        client.interaction(interaction.clone().application_id).create_response(
            interaction.clone().id,
            &interaction.token,
            &InteractionResponse {
                kind: InteractionResponseType::ChannelMessageWithSource,
                data: Some(InteractionResponseDataBuilder::new()
                    .content("Command must be used in league category")
                    .flags(MessageFlags::EPHEMERAL)
                    .build()
                )
            }
        )
            .await?;
    }
    Ok(())
}

async fn ping(interaction: Interaction, client: Arc<Client>) -> Result<()> {
    let _ = &client.clone()
        .interaction(interaction.application_id)
        .create_response(interaction.id, &interaction.token, &InteractionResponse {
            kind: InteractionResponseType::ChannelMessageWithSource,
            data: Some(InteractionResponseDataBuilder::new()
                .content("pong")
                .build()
            ),
        })
        .await?;
    Ok(())
}

async fn role(interaction: Interaction, client: Arc<Client>, data: CommandData, pool: Arc<Pool<Sqlite>>) -> Result<()> {

    let row = &data.clone().resolved.unwrap().roles.iter().next().unwrap().0.to_owned();

    //let role_id: Id<RoleMarker> = &data.options.iter().next().unwrap().value.to_owned();
    match &data.options.get(0).unwrap().name.as_str() {
        &"delete" => {
            let mut tx = pool.clone().as_ref().begin().await?;
            match sqlx::query("UPDATE role_info SET INFO = '' WHERE id = ?")
                .bind(row.get().to_string())
                .execute(&mut *tx)
                .await?
                .rows_affected() {

                1 => {
                    client
                        .interaction(interaction.application_id)
                        .create_response(interaction.id, &interaction.token, &InteractionResponse {
                            kind: InteractionResponseType::ChannelMessageWithSource,
                            data: Some(
                                InteractionResponseDataBuilder::new()
                                    .embeds(vec![
                                        EmbedBuilder::new()
                                            .title("Role Deleted From Database")
                                            .build()
                                    ])
                                    .flags(MessageFlags::EPHEMERAL)
                                    .build()
                            ),
                        })
                        .await?;
                },
                _ => {
                    tracing::warn!("Zero rows affected on a `role delete` command {}", &row.get().to_string());
                    client
                        .interaction(interaction.application_id)
                        .create_response(interaction.id, &interaction.token, &InteractionResponse {
                            kind: InteractionResponseType::ChannelMessageWithSource,
                            data: Some(
                                InteractionResponseDataBuilder::new()
                                    .embeds(vec![
                                        EmbedBuilder::new()
                                            .title("Zero rows affected. Consider this an error")
                                            .description("Consider this an error. It has been logged, but feel free to ping @sadpuppies")
                                            .build()
                                    ])
                                    .flags(MessageFlags::EPHEMERAL)
                                    .build()
                            ),
                        })
                        .await?;
                }
            }
            tx.commit().await?;
        },
        &"edit" => {
            let mut tx = pool.clone().as_ref().begin().await?;

            match sqlx::query("SELECT * FROM role_info WHERE id = ?")
                .bind(&row.get().to_string())
                .fetch_optional(&mut *tx)
                .await? {

                Some(row) => {

                    let component = Component::ActionRow(ActionRow {
                        components: vec![
                            Component::TextInput(TextInput {
                                custom_id: format!("role_edit_{}", &row.get::<String, usize>(0).to_string()),
                                label: "edit".to_string(),
                                max_length: Some(1024),
                                min_length: Some(20),
                                placeholder: None,
                                required: None,
                                style: TextInputStyle::Paragraph,
                                value: None,
                            })
                        ]
                    });

                    client
                        .interaction(interaction.application_id)
                        .create_response(interaction.id, &interaction.token, &InteractionResponse {
                            kind: InteractionResponseType::Modal,
                            data: Some(InteractionResponseData {
                                components: Some(vec![component]),
                                custom_id: Some(format!("role_edit_{}", &row.get::<String, usize>(0).to_string()).to_string()),
                                title: Some(String::from("modal")),
                                ..Default::default()
                            }),
                        })
                        .await?;
                },
                None => {
                    tracing::warn!("Zero rows affected on a `role edit` command {}", &row.get().to_string());
                    client
                        .interaction(interaction.application_id)
                        .create_response(interaction.id, &interaction.token, &InteractionResponse {
                            kind: InteractionResponseType::ChannelMessageWithSource,
                            data: Some(
                                InteractionResponseDataBuilder::new()
                                    .embeds(vec![
                                        EmbedBuilder::new()
                                            .title(format!("Role Not Found in Database: {}", &row.to_string()))
                                            .description("Consider this an error. It has been logged, but feel free to ping @sadpuppies")
                                            .build()
                                    ])
                                    .flags(MessageFlags::EPHEMERAL)
                                    //.components()
                                    .build()
                            ),
                        })
                        .await?;
                }

            }

            tx.commit().await?;
        },
        &"info" => {
            let mut tx = pool.clone().as_ref().begin().await?;

            match sqlx::query("SELECT * FROM role_info WHERE id = ?")
                .bind(row.get().to_string())
                .fetch_optional(&mut *tx)
                .await? {

                Some(row) => {
                    client
                        .interaction(interaction.application_id)
                        .create_response(interaction.id, &interaction.token, &InteractionResponse {
                            kind: InteractionResponseType::ChannelMessageWithSource,
                            data: Some(
                                InteractionResponseDataBuilder::new()
                                    .content(format!("<@&{}>", &row.get::<String, usize>(0)))
                                    .embeds(vec![
                                        EmbedBuilder::new()
                                            .title("Role Info")
                                            .description(format!("ID: {}\nInfo: {}\nAdded By: <@!{}>\nDate Modified: <t:{}:F>",
                                                                 &row.get::<String, usize>(0),
                                                                 &row.get::<String, usize>(1),
                                                                 &row.get::<String, usize>(2),
                                                                 &row.get::<String, usize>(3)
                                            ))
                                            .build()
                                    ])
                                    .flags(MessageFlags::EPHEMERAL)
                                    //.components()
                                    .build()
                            ),
                        })
                        .await?;
                }
                None => {
                    client
                        .interaction(interaction.application_id)
                        .create_response(interaction.id, &interaction.token, &InteractionResponse {
                            kind: InteractionResponseType::ChannelMessageWithSource,
                            data: Some(
                                InteractionResponseDataBuilder::new()
                                    .embeds(vec![
                                        EmbedBuilder::new()
                                            .title(format!("Role Not Found in Database: {}", &row.to_string()))
                                            .description("Consider this an error. It has been logged, but feel free to ping @sadpuppies")
                                            .build()
                                    ])
                                    .flags(MessageFlags::EPHEMERAL)
                                    //.components()
                                    .build()
                            ),
                        })
                        .await?;
                }
            }

            tx.commit().await?;
        },
        _ => ()
    }

    Ok(())
}

async fn roll(interaction: Interaction, client: Arc<Client>, data: CommandData) -> Result<()> {
    let client = client.interaction(interaction.application_id);

    let mut acc: [i64; 6] = [0, 0, 0, 0, 0, 0];
    if let CommandOptionValue::Integer(n) = data.options.iter().next().unwrap().value.to_owned() {
        for _ in 0..n {
            let i: usize = Uniform::from(0..=5)
                .sample(&mut thread_rng());
            acc[i] += 1;
        }
    }
    let mut i: i64 = 0;
    for item in acc {
        i += item
    }

    let user = interaction.member.unwrap().user.expect("No username supplied").name;
    let title = format!("{}'s Dice", user.to_string());

    let desc = format!("<:dice_one:665298149274943550> {}\
    \n<:dice_two:711992588709920779> {}\
    \n<:dice_three:711992763209744415> {}\
    \n<:dice_four:711992276724744253> {}\
    \n<:dice_five:711992019349799005> {}\
    \n<:dice_six:665298304732495892> {}\
    \n\n**Total**: {}",
                       acc[0].to_string(),
                       acc[1].to_string(),
                       acc[2].to_string(),
                       acc[3].to_string(),
                       acc[4].to_string(),
                       acc[5].to_string(),
                       i.to_string()
    );

    let e = EmbedBuilder::new()
        .title(title)
        .description(desc)
        //.image(&user.avatar) ///No reason to continue looking into this until I can turn the bytes into something useful
        .validate()?
        .build();

    let res = InteractionResponseDataBuilder::new().embeds([e]);

    let response = InteractionResponse {
        kind: InteractionResponseType::ChannelMessageWithSource,
        data: Some(res.build()),
    };

    client
        .create_response(interaction.id, &interaction.token, &response)
        .await?;
    Ok(())
}

async fn sides(interaction: Interaction, client: Arc<Client>, data: CommandData) -> Result<()> {
    let client = client.interaction(interaction.application_id);

    let mut array = Vec::new();
    let chunks = data.resolved.unwrap().users;
    for item in chunks {
        array.push(item.0.get())
    }
    array.shuffle(&mut thread_rng());

    let user = interaction.member.unwrap().user.expect("No username supplied").name;
    let title = format!("{}'s Sides", user.to_string());

    let desc: String = match &array.len() {
        2 => format!(
            "Axis: <@!{}>\nAllies: <@!{}>",
            array.get(0).unwrap(),
            array.get(1).unwrap()
        ),
        3 => format!(
            "Allies: <@!{}>\nGermany: <@!{}>\nJapan: <@!{}>",
            array.get(0).unwrap(),
            array.get(1).unwrap(),
            array.get(2).unwrap()
        ),
        4 => format!(
            "USSR-USA: <@!{}>\nGermany: <@!{}>\nUK: <@!{}>\nJapan: <@!{}>",
            array.get(0).unwrap(),
            array.get(1).unwrap(),
            array.get(2).unwrap(),
            array.get(3).unwrap()
        ),
        5 => format!(
            "USSR: <@!{}>\nGermany: <@!{}>\nUK: <@!{}>\nJapan: <@!{}>\nUSA: <@!{}>",
            array.get(0).unwrap(),
            array.get(1).unwrap(),
            array.get(2).unwrap(),
            array.get(3).unwrap(),
            array.get(4).unwrap()
        ),
        _ => "Error".to_string()
    };

    let e = EmbedBuilder::new()
        .title(title)
        .description(desc)
        //.image(&user.avatar) ///No reason to continue looking into this until I can turn the bytes into something useful
        .validate()?
        .build();

    let res = InteractionResponseDataBuilder::new().embeds([e]);

    let response = InteractionResponse {
        kind: InteractionResponseType::ChannelMessageWithSource,
        data: Some(res.build()),
    };

    client
        .create_response(interaction.id, &interaction.token, &response)
        .await?;
    Ok(())
}

async fn timeout(interaction: Interaction, client: Arc<Client>, data: CommandData) -> Result<()> {
    let staff_role: Id<RoleMarker> = Id::new(943827276104097842);
    if interaction.clone().member.unwrap().roles.contains(&staff_role) {
        let guild = interaction.guild_id.unwrap();
        let target: Id<UserMarker> = data.target_id.unwrap().cast();
        let now: i64 = SystemTime::now().duration_since(UNIX_EPOCH)?.as_secs().try_into().unwrap();
        let one_day_from_now = 24 * 60 * 60 + now;
        if Some(client
            .update_guild_member(guild, target)
            .communication_disabled_until(Timestamp::from_secs(one_day_from_now).unwrap().into())?
            .await?
            .model()
            .await?
            .communication_disabled_until).is_some()
        {
            let foo = Component::ActionRow(ActionRow {
                components: Vec::from([Component::Button(Button {
                    custom_id: Some(format!("undo_timeout_{}", target).to_owned()),
                    disabled: false,
                    emoji: None,
                    label: Some("Undo Timeout".to_owned()),
                    style: ButtonStyle::Primary,
                    url: None,
                })]),
            });
            client.interaction(interaction.clone().application_id).create_response(
                interaction.clone().id,
                &interaction.token,
                &InteractionResponse {
                    kind: InteractionResponseType::ChannelMessageWithSource,
                    data: Some(InteractionResponseDataBuilder::new()
                        .content("User is in timeout for 24 hours")
                        .components([foo])
                        .flags(MessageFlags::EPHEMERAL)
                        .build()
                    )
                }
            )
                .await?;
        }
        else {
            client.interaction(interaction.clone().application_id).create_response(
                interaction.clone().id,
                &interaction.token,
                &InteractionResponse {
                    kind: InteractionResponseType::ChannelMessageWithSource,
                    data: Some(InteractionResponseDataBuilder::new()
                        .content("Unknown failure")
                        .flags(MessageFlags::EPHEMERAL)
                        .build()
                    )
                }
            )
                .await?;
        }
    }
    else {
        client.interaction(interaction.clone().application_id).create_response(
            interaction.clone().id,
            &interaction.token,
            &InteractionResponse {
                kind: InteractionResponseType::ChannelMessageWithSource,
                data: Some(InteractionResponseDataBuilder::new()
                    .content("Only Staff can use this command. **For Shame!**")
                    .flags(MessageFlags::EPHEMERAL)
                    .build()
                )
            }
        )
            .await?;
    }
    Ok(())
}

async fn undo_timeout(interaction: Interaction, client: Arc<Client>, data: MessageComponentInteractionData) -> Result<()> {
    let uid = Id::new(data.custom_id.get(13..).unwrap().parse().unwrap());
    let guild = interaction.guild_id.unwrap();
    if Some(client
        .update_guild_member(guild, uid)
        .communication_disabled_until(None)?
        .await?
        .model()
        .await?
        .communication_disabled_until).is_some()
    {
        client.interaction(interaction.clone().application_id).create_response(
            interaction.clone().id,
            &interaction.token,
            &InteractionResponse {
                kind: InteractionResponseType::ChannelMessageWithSource,
                data: Some(InteractionResponseDataBuilder::new()
                    .content("User removed from timeout for 24 hours")
                    .flags(MessageFlags::EPHEMERAL)
                    .build()
                )
            }
        )
            .await?;
    }

    Ok(())
}
