// Using cache might cause panic when first starting, before the cache is populated
// This can be ignored, or add messages to the cache at start time (on ready event)
// or add additional error handling

use anyhow::Result;
use regex_lite::Regex;
use std::sync::Arc;
use twilight_cache_inmemory::InMemoryCache;
use twilight_http::Client;
use twilight_model::{
    channel::message::{
        Embed,
        MessageFlags
    },
    gateway::payload::incoming::MessageUpdate,
    id::{
        Id,
        marker::ChannelMarker
    }
};
use twilight_util::{
    builder::embed::{
        EmbedBuilder,
        EmbedFieldBuilder,
        EmbedFooterBuilder,
        ImageSource
    },
    snowflake::Snowflake
};

pub async fn message_update(message: &Box<MessageUpdate>, client: &Arc<Client>, cache: &Arc<InMemoryCache>, pattern: Regex) -> Result<()> {
    let server_logs_channel: Id<ChannelMarker> = Id::new(941854510647762954);
    match &message.channel_id {
        x if x != &server_logs_channel => {
            let author = message.author.clone();
            match author {
                Some(author) => {
                    if !author.bot {
                        let message_content = &message.content;
                        match message_content {
                            Some(message_content) => {
                                if message_content.contains("<@644511391302025226>") || message_content.contains("<@!644511391302025226>") { // @sadpuppies user mention and user id mention
                                    client
                                        .create_message(message.clone().channel_id)
                                        .reply(message.clone().id)
                                        .content("Did you mean <@!948208421054865410>?")?
                                        .await?;
                                }
                                match &message.flags {
                                    Some(MessageFlags::SUPPRESS_EMBEDS) => (),
                                    _ => {
                                        if pattern.is_match(&message_content) {
                                            client
                                                .update_message(message.channel_id, message.id)
                                                .flags(MessageFlags::SUPPRESS_EMBEDS)
                                                .await?;
                                        }
                                    }
                                };
                            }
                            None => ()
                        }

                        let thumbnail = match author.avatar {
                            Some(avatar) => {
                                let thumbnail = ImageSource::url(format!(
                                    "https://cdn.discordapp.com/avatars/{}/{}.png?size=512",
                                    &author.id.id(),
                                    &avatar
                                ))?;
                                thumbnail
                            }
                            None => {
                                let thumbnail = ImageSource::url(
                                    "https://cdn.discordapp.com/embed/avatars/1.png"
                                )?;
                                thumbnail
                            }
                        };

                        let mut e = EmbedBuilder::new()
                            .title("Message Edited")
                            .description(format!("[Jump to message](https://discord.com/channels/{}/{}/{})", message.guild_id.unwrap().id(), message.channel_id, message.id))
                            .thumbnail(thumbnail);
                        match &message.author {
                            Some(author) => {
                                e = e
                                    .field(EmbedFieldBuilder::new("Author", format!("<@!{}>", author.id.id())).inline().build())
                            },
                            _ => ()
                        };
                        let mut items: Vec<String> = Vec::new();
                        match &message.attachments {
                            Some(attachments) => {
                                e = e
                                    .field(EmbedFieldBuilder::new("Attachments", format!("{}", attachments.len())).inline().build());
                                for item in attachments {
                                    items.push(item.url.to_string())
                                }
                            },
                            _ => {}
                        }
                        e = e
                            .field(EmbedFieldBuilder::new("Channel", format!("<#{}>", &message.channel_id)).inline().build());

                        if &cache.message(message.id).unwrap().content().len() > &1024 {
                            cache.message(message.id).unwrap().content().to_owned().truncate(1024)
                        }
                        e = e
                            .field(EmbedFieldBuilder::new("Before Content", format!("{}", &cache.message(message.id).unwrap().content())).build());

                        if message.clone().content.is_some() {
                            if message.clone().content.unwrap().len() > 1024 {
                                message.clone().content.unwrap().truncate(1024)
                            }
                            e = e
                                .field(EmbedFieldBuilder::new("After Content", format!("{}", &message.content.clone().unwrap())).build());
                        }
                        if message.edited_timestamp.is_some() {
                            e = e
                                .field(EmbedFieldBuilder::new("Before Timestamp", format!("<t:{}:F>", &cache.message(message.id).unwrap().timestamp().as_secs())).inline().build())
                                .field(EmbedFieldBuilder::new("After Timestamp", format!("<t:{}:F>", &message.edited_timestamp.expect("f").as_secs())).inline().build())
                                .footer(EmbedFooterBuilder::new(format!("Author: {} • Channel: {} • Message: {}", &message.author.clone().expect("f").id, message.channel_id, message.id)));
                        }
                        let e: Embed = e
                            .validate()?
                            .build();
                        let default_message = client
                            .create_message(server_logs_channel)
                            .embeds(&[e])?
                            .await?
                            .model()
                            .await
                            .unwrap()
                            .id;
                        if items.len() > 0 {
                            for item in items {
                                client
                                    .create_message(server_logs_channel)
                                    .reply(default_message)
                                    .content(item.as_str())?
                                    .await?;
                            }
                        }
                    }
                },
                None => ()
            }
        },
        _ => ()
    }
    Ok(())
}