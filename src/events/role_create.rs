use anyhow::Result;
use std::{
    sync::Arc,
    time::{SystemTime, UNIX_EPOCH}
};
use sqlx::{
    FromRow,
    Pool,
    Sqlite
};
use twilight_http::Client;
use twilight_model::{
    id::{
        Id,
        marker::ChannelMarker
    },
    channel::message::{
        component::{
            ActionRow,
            Button,
            ButtonStyle
        },
        Component
    },
    gateway::payload::incoming::RoleCreate
};
use twilight_util::snowflake::Snowflake;

pub async fn role_create(role: &RoleCreate, client: &Arc<Client>, pool: Arc<Pool<Sqlite>>) -> Result<()> {

    tracing::info!("role_create initiated: {:?}", &role);

    #[derive(FromRow, Debug)]
    struct RoleInfo { //currently this is the only usage, maybe useful to put it elsewhere too
        id: String,
        date_updated: String // This must be a String because sqlite does not have a u64 type and I cannot cast a u64 to i64
    }

    let data = RoleInfo {
        id: role.role.id.id().to_string(),
        date_updated: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs().to_string(),
    };

    let mut tx = pool.clone().as_ref().begin().await?;

    sqlx::query("INSERT INTO role_info (id, date_updated) VALUES (?, ?)")
        .bind(data.id.to_string())
        .bind(data.date_updated)
        .execute(&mut *tx)
        .await?;

    tx.commit().await?;


    let server_commands_channel: Id<ChannelMarker> = Id::new(670090977356021780);

    let component = Component::ActionRow(ActionRow {
        components: vec![
            Component::Button(Button {
                custom_id: Some(format!("role_desc_{}", &data.id)),
                disabled: false,
                emoji: None,
                label: Some("Add Role Description".to_string()),
                style: ButtonStyle::Primary,
                url: None,
            })
        ]
    });

    client
        .create_message(server_commands_channel)
        .components(
            &[component]
        )?
        .content(&*format!("New role: <@&{}>", &data.id))?
        .await?;

    tracing::info!("role_create completed: {:?}", &role);

    Ok(())
}
