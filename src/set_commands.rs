use anyhow::Result;
use twilight_http::client::InteractionClient;
use twilight_model::{
    application::command::{
        CommandOption,
        CommandOptionType,
        CommandType
    },
    guild::Permissions,
    id::{
        Id,
        marker::GuildMarker
    }
};
use twilight_util::builder::command::{
    CommandBuilder,
    IntegerBuilder,
    UserBuilder
};

pub async fn set_commands(interaction_client: InteractionClient<'_>) -> Result<()> {

    let guild: Id<GuildMarker> = Id::new(606254910438375434);
    let assign_to_league = CommandBuilder::new("Assign to League", "", CommandType::User)
        .guild_id(guild)
        .nsfw(false)
        .validate()?
        .build();
    let assign_to_blitz_tournament = CommandBuilder::new("Assign to Blitz Tournament", "", CommandType::User)
        .guild_id(guild)
        .nsfw(false)
        .validate()?
        .build();
    let assign_to_blitz_tournament_showcase = CommandBuilder::new("Assign to Blitz Showcase", "", CommandType::User)
        .guild_id(guild)
        .nsfw(false)
        .validate()?
        .build();
    let delete_message = CommandBuilder::new("Delete Message", "", CommandType::Message)
        .guild_id(guild)
        .nsfw(false)
        .validate()?
        .build();
    let mute_league = CommandBuilder::new("Mute League", "", CommandType::Message)
        .guild_id(guild)
        .nsfw(false)
        .validate()?
        .build();
    let ping = CommandBuilder::new("ping", "basic ping pong functionality", CommandType::ChatInput)
        .guild_id(guild)
        .nsfw(false)
        .validate()?
        .build();
    let role = CommandBuilder::new("role", "control the role database", CommandType::ChatInput)
        .guild_id(guild)
        .nsfw(false)
        .default_member_permissions(Permissions::MANAGE_ROLES)
        .option(CommandOption {
            autocomplete: None,
            channel_types: None,
            choices: None,
            description: "delete the role info".to_string(),
            description_localizations: None,
            kind: CommandOptionType::SubCommand,
            max_length: None,
            max_value: None,
            min_length: None,
            min_value: None,
            name: "delete".to_string(),
            name_localizations: None,
            options: Some(
                vec![
                    CommandOption {
                        autocomplete: None,
                        channel_types: None,
                        choices: None,
                        description: "which role to delete info".to_string(),
                        description_localizations: None,
                        kind: CommandOptionType::Role,
                        max_length: None,
                        max_value: None,
                        min_length: None,
                        min_value: None,
                        name: "role".to_string(),
                        name_localizations: None,
                        options: None,
                        required: Some(true),
                    }
                ]
            ),
            required: None,
        })
        .option(CommandOption {
            autocomplete: None,
            channel_types: None,
            choices: None,
            description: "display the role info".to_string(),
            description_localizations: None,
            kind: CommandOptionType::SubCommand,
            max_length: None,
            max_value: None,
            min_length: None,
            min_value: None,
            name: "info".to_string(),
            name_localizations: None,
            options: Some(
                vec![
                    CommandOption {
                        autocomplete: None,
                        channel_types: None,
                        choices: None,
                        description: "which role to display info".to_string(),
                        description_localizations: None,
                        kind: CommandOptionType::Role,
                        max_length: None,
                        max_value: None,
                        min_length: None,
                        min_value: None,
                        name: "role".to_string(),
                        name_localizations: None,
                        options: None,
                        required: Some(true),
                    }
                ]
            ),
            required: None,
        })
        .option(CommandOption {
            autocomplete: None,
            channel_types: None,
            choices: None,
            description: "edit the role info".to_string(),
            description_localizations: None,
            kind: CommandOptionType::SubCommand,
            max_length: None,
            max_value: None,
            min_length: None,
            min_value: None,
            name: "edit".to_string(),
            name_localizations: None,
            options: Some(
                vec![
                    CommandOption {
                        autocomplete: None,
                        channel_types: None,
                        choices: None,
                        description: "which role to edit info".to_string(),
                        description_localizations: None,
                        kind: CommandOptionType::Role,
                        max_length: None,
                        max_value: None,
                        min_length: None,
                        min_value: None,
                        name: "role".to_string(),
                        name_localizations: None,
                        options: None,
                        required: Some(true),
                    }
                ]
            ),
            required: None,
        })
        .validate()?
        .build();
    let roll = CommandBuilder::new("roll", "roll a d6", CommandType::ChatInput)
        .guild_id(guild)
        .nsfw(false)
        .default_member_permissions(Permissions::empty())
        .option(IntegerBuilder::new("amount", "amount of dice to roll")
            .autocomplete(false)
            .min_value(1)
            .max_value(1024)
            .required(true)
            .build())
        .validate()?
        .build();
    let sides = CommandBuilder::new("sides", "choose sides for a multiplayer game", CommandType::ChatInput)
        .guild_id(guild)
        .nsfw(false)
        .option(UserBuilder::new("player_one", "player one required")
            .required(true)
            .build())
        .option(UserBuilder::new("player_two", "player two required")
            .required(true)
            .build())
        .option(UserBuilder::new("player_three", "player three not required")
            .required(false)
            .build())
        .option(UserBuilder::new("player_four", "player four not required")
            .required(false)
            .build())
        .option(UserBuilder::new("player_five", "player five not required")
            .required(false)
            .build())
        .validate()?
        .build();
    let timeout = CommandBuilder::new("Timeout", "", CommandType::User)
        .guild_id(guild)
        .nsfw(false)
        .validate()?
        .build();

    let commands = [
        assign_to_league.into(),
        assign_to_blitz_tournament.into(),
        assign_to_blitz_tournament_showcase.into(),
        delete_message.into(),
        mute_league.into(),
        ping.into(),
        role.into(),
        roll.into(),
        sides.into(),
        timeout.into()
    ];
    if let Err(error) = interaction_client.set_guild_commands(guild, &commands).await {
        tracing::error!(?error, "failed to register commands");
    }
    Ok(())
}